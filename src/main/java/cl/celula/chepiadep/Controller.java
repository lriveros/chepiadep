/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.celula.chepiadep;

import cl.celula.servicios.dto.ResultadoPyme;
import cl.celula.util.servicios.ClientesPymeWS;
import cl.celula.util.servicios.ClientePersonasWS;

import org.apache.commons.codec.binary.Base64;
/**
 *
 * @author Luis Riveros
 */
public class Controller {
    
    public String imprimir(String texto, String serv, String process, boolean base64) throws Exception{
        String respuesta = "";
        ClientesPymeWS cws = new ClientesPymeWS();
        ClientePersonasWS cpws = new ClientePersonasWS();
        try{
            ResultadoPyme r = new ResultadoPyme();
            String url = serv.concat(process);
                        
            if(null != process)switch (process) {
                case Constantes.WSDL_PROCESS_KIT_PYME:
                    r = cws.obtenerKitOriginales(url, texto);
                    break;
                case Constantes.WSDL_PROCESS_KIT_PYME_COPIAS:
                    r = cws.obtenerKitOriginales(url, texto);
                    break;
                case Constantes.WSDL_PROCESS_CREDITO_PYME:
                    r = cws.obtenerCreditoOriginales(url, texto);
                    break;
                case Constantes.WSDL_PROCESS_CREDITO_PYME_COPIAS:
                    r = cws.obtenerCreditoCopias(url, texto);
                    break;
                case Constantes.WSDL_PROCESS_CHECKLIST:
                    r.setStrEncode(cws.obtenerChecklist(url, texto));
                    break;
                case Constantes.WSDL_PROCESS_PER_CREDITO_CUOTAS:
                    if(!base64){
                        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
                        texto = new String(bytesEncoded);
                    }
                    break;
                case Constantes.WSDL_PROCESS_PER_CTA_CTE_MN:
                    if(!base64){
                        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
                        texto = new String(bytesEncoded);
                    }
                    break;
                case Constantes.WSDL_PROCESS_PER_CTA_CTE_MX:
                    if(!base64){
                        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
                        texto = new String(bytesEncoded);
                    }
                    break;
                case Constantes.WSDL_PROCESS_PER_CTA_VISTA_COPIAS:
                    if(!base64){
                        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
                        texto = new String(bytesEncoded);
                    }
                    r = cpws.obtenerProductoCuentaVistaCopias(url, texto);
                    break;
                case Constantes.WSDL_PROCESS_PER_CTA_VISTA_ORIGINALES:
                    if(!base64){
                        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
                        texto = new String(bytesEncoded);
                    }
                    r = cpws.obtenerProductoCuentaVistaOriginales(url, texto);
                    break;
                case Constantes.WSDL_PROCESS_PER_KIT_BANCO:
                    if(!base64){
                        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
                        texto = new String(bytesEncoded);
                    }
                    r = cpws.obtenerProductoKitBanco(url, texto);
                    break;
                case Constantes.WSDL_PROCESS_PER_KIT_CLIENTE:
                    if(!base64){
                        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
                        texto = new String(bytesEncoded);
                    }
                    r = cpws.obtenerProductoKitCliente(url, texto);
                    break;
                case Constantes.WSDL_PROCESS_PER_CREDITO_CONSUMO_COPIAS:
                    if(!base64){
                        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
                        texto = new String(bytesEncoded);
                    }
                    break;
                case Constantes.WSDL_PROCESS_PER_CREDITO_CONSUMO_ORIGINALES:
                    if(!base64){
                        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
                        texto = new String(bytesEncoded);
                    }
                    break;
                case Constantes.WSDL_PROCESS_PER_TARJETA_CREDITO:
                    if(!base64){
                        byte[] bytesEncoded = Base64.encodeBase64(texto.getBytes());
                        texto = new String(bytesEncoded);
                    }
                    break;
                default:
                    break;
            }
            
            respuesta = r.getStrEncode();
        }catch(Exception e){
            throw e;
        }
        return respuesta;
    }
    
}
