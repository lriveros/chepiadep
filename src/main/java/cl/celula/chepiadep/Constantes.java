/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.celula.chepiadep;

/**
 *
 * @author ImageMaker
 */
public class Constantes {
        
    public static final String WSDL_PROCESS_CREDITO_PYME = "/soap/services/BCH_PLATDOCPYME_PROCESS/process/processProductosPYMEOriginales?wsdl";
    public static final String WSDL_PROCESS_CREDITO_PYME_COPIAS = "/soap/services/BCH_PLATDOCPYME_PROCESS/process/processProductosPYMECopias?wsdl";
    public static final String WSDL_PROCESS_CHECKLIST = "/soap/services/BCH_CHECKLIST_ADEP/process/processCheckListDocumentos?wsdl";

    public static final String WSDL_PROCESS_KIT_PYME = "/soap/services/BCH_PLATDOCPYME_PROCESS/process/processKitPYMEOriginales?wsdl";
    public static final String WSDL_PROCESS_KIT_PYME_COPIAS = "/soap/services/BCH_PLATDOCPYME_PROCESS/process/processKitPYMECopias?wsdl";

    public static final String WSDL_PROCESS_PER_CREDITO_CUOTAS = "/soap/services/BancaPersonaBCH/process/processProductoCreditoCuotas?wsdl";
    public static final String WSDL_PROCESS_PER_CTA_CTE_MN = "/soap/services/BancaPersonaBCH/process/processProductoCuentaCorrienteMN?wsdl";
    public static final String WSDL_PROCESS_PER_CTA_CTE_MX = "/soap/services/BancaPersonaBCH/process/processProductoCuentaCorrienteMX?wsdl";
    public static final String WSDL_PROCESS_PER_CTA_VISTA_COPIAS= "/soap/services/BancaPersonaBCH/process/processProductoCuentaVistaCopias?wsdl";
    public static final String WSDL_PROCESS_PER_CTA_VISTA_ORIGINALES = "/soap/services/BancaPersonaBCH/process/processProductoCuentaVistaOriginales?wsdl";
    public static final String WSDL_PROCESS_PER_KIT_BANCO= "/soap/services/BancaPersonaBCH/process/processProductoKitBanco?wsdl";
    public static final String WSDL_PROCESS_PER_KIT_CLIENTE = "/soap/services/BancaPersonaBCH/process/processProductoKitCliente?wsdl";
    public static final String WSDL_PROCESS_PER_CREDITO_CONSUMO_COPIAS = "/soap/services/BancaPersonaBCH/process/processProductoKitCreditoConsumoCliente?wsdl";
    public static final String WSDL_PROCESS_PER_CREDITO_CONSUMO_ORIGINALES = "/soap/services/BancaPersonaBCH/process/processProductoKitCreditoConsumoBanco?wsdl";
    public static final String WSDL_PROCESS_PER_TARJETA_CREDITO = "/soap/services/BancaPersonaBCH/process/processProductoTarjetaCredito?wsdl";
    
    public static final String SERV_DESA = "http://200.14.166.221:8013";
    public static final String SERV_QA1 = "http://200.14.165.235:8011";
    public static final String SERV_QA2 = "http://200.14.165.229:8013";
}
